package utils;

import static io.restassured.RestAssured.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.http.HttpStatus;

import com.github.javafaker.Faker;
import com.google.gson.Gson;

import simulacoes.Simulacoes;

public class SimulacoesDados {

	private final Faker faker;
	private RestricaoCPF restricaoCpf;
	private final Gson gson;
	private List<Simulacoes> simulacoes;

	public SimulacoesDados() {
		faker = new Faker();
		restricaoCpf = new RestricaoCPF();
		gson = new Gson();
		simulacoes = new ArrayList<>();
		setSimulationDTO();
	}

	public List<Simulacoes> getSimulacoes() {
		return simulacoes;
	}

	public Simulacoes selectRandomSimulationInDatabase() {
		int index = (int) (Math.random() * simulacoes.size());
		return simulacoes.get(index);
	}

	public String getNewCpf() {
		return RestricaoCPF.getRandomCpf();
	}

	public String getNewName() {
		return faker.name().firstName();
	}

	public double getNewValue() {
		return faker.number().randomDouble(2, 1000, 40000);
	}

	public String getNewEmail() {
		return faker.internet().emailAddress();
	}

	public int getNewPortion() {
		return faker.number().numberBetween(2, 48);
	}

	public boolean getNewInsurance() {
		return faker.bool().bool();
	}

	public Simulacoes jsonToObject(String json) {
		return gson.fromJson(json, Simulacoes.class);
	}

	private void setSimulationDTO() {
		simulacoes = Arrays.asList(getSimulationsInDataBase());
	}

	public String generateNonExistentCpf() {
		String cpf = RestricaoCPF.getRandomCpf();
		List<String> cpfs = simulacoes.stream().map(p -> p.getCpf()).collect(Collectors.toList());
		while (cpfs.contains(cpf)) {
			cpf = RestricaoCPF.getRandomCpf();
		}
		return cpf;
	}

	public int getNonExistentID() {
		Random random = new Random();
		int randomID = random.nextInt();
		List<Integer> ids = simulacoes.stream().map(p -> p.getId()).collect(Collectors.toList());
		while (ids.contains(randomID)) {
			randomID = random.nextInt();
		}
		return randomID;
	}

	public Simulacoes[] getSimulationsInDataBase() {
		return when().get("/simulacoes").then().statusCode(HttpStatus.SC_OK).extract().as(Simulacoes[].class);

	}

	public Simulacoes getNewSimulation() {
		String name = getNewName();
		String cpf = getNewCpf();
		String email = getNewEmail();
		double value = getNewValue();
		int portion = getNewPortion();
		boolean insurance = getNewInsurance();
		return new Simulacoes(cpf, name, email, value, portion, insurance);
	}

	/*
	 * -----------------------------------------------------------------------------
	 */
	/* Construtores */
	public SimulacoesDados(String cpf, String name, String email, double value, int portion, boolean insurance) {
		this.faker = new Faker();
		this.gson = new Gson();

	}
	
	private int getId() {
		return 0;
	}

	public Simulacoes generateNewSimulation() {
		return null;
	}

	public String getCpf() {
		return null;
	}

	public String getRandomCpf() {
		return null;
	}
}

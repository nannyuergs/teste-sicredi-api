package utils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.github.javafaker.Faker;

public class RestricaoCPF {
	private final static Faker faker = new Faker();

    public static String getRandomCpf() {
        return String.valueOf(faker.number().randomNumber(11, false));
    }

    public static String getCpfComRestricao() {

        List<String> cpfComRestricao =
                Arrays.asList(
                        "97093236014",
                        "60094146012",
                        "84809766080",
                        "62648716050",
                        "26276298085",
                        "01317496094",
                        "55856777050",
                        "19626829001",
                        "24094592008",
                        "58063164083"
                );

        String randomCpf = cpfComRestricao.get(new Random().nextInt(cpfComRestricao.size()));

        return randomCpf;
    }


}

package utils;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentReport {

	static ExtentTest test;
	static ExtentReports report;

	public static String dataHoraParaArquivo() {

		Timestamp ts = new Timestamp(System.currentTimeMillis());
		return new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(ts);

	}

	public static String escreveRelatorio() {

		report = new ExtentReports(System.getProperty("user.dir") + "\\relatorios\\Relatorio"
				+ utils.ExtentReport.dataHoraParaArquivo() + ".html", true);
		report.loadConfig(new File(System.getProperty("user.dir") + "\\relatorios\\configuracao\\extent-config.xml"));
		return escreveRelatorio();

	}

}

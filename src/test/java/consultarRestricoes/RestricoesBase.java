package consultarRestricoes;

import api.AcessoApi;
import utils.RestricaoCPF;

public class RestricoesBase {

	public abstract class RestricaoBase extends AcessoApi {

		public static final String BASE_PATH_RESTRICTION = "restricoes/{cpf}";
		protected final RestricaoCPF restricaoCPF;

		public RestricaoBase() {
			restricaoCPF = new RestricaoCPF();
		}
	}
}

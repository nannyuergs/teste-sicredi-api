package consultarRestricoes;

import static io.restassured.RestAssured.given;

import java.io.File;

import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runners.MethodSorters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.RestricaoCPF;

//Ordenar os testes com Junit
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultarRestricoes extends RestricoesBase {

	RequestSpecification request;

	public static final String RESTRICOES_CPF = "restricoes/{cpf}";

	static ExtentTest test;
	static ExtentReports report;

	@BeforeClass
	public static void startTest() {
		report = new ExtentReports(System.getProperty("user.dir") + "/relatorios/Relatorio_"
				+ utils.ExtentReport.dataHoraParaArquivo() + ".html", true);
		report.loadConfig(new File(System.getProperty("user.dir") + "/relatorios/configuracao/extent-config.xml"));

	}

	@Test
	public void CT01_validaCpfComRestricao() {

		test = report.startTest("CT01_validaCpfComRestricao");

		System.out.println("\n");
		System.out.println("#### CT01: Exibe CPF com Restricao ####\n");

		// Given()
		request = RestAssured.given().log().all();
		String cpf = RestricaoCPF.getCpfComRestricao();
		Response response = getRestricaoResponse(cpf);
		// when()
		String message = response.jsonPath().getString("mensagem");
		// Then()
		Assertions.assertEquals(HttpStatus.SC_OK, response.statusCode());
		Assertions.assertEquals(String.format("O CPF %s possui restricao", cpf), message);
	}

	@Test
	public void CT02_validateCpfSemRestricao() {

		test = report.startTest("CT02_validaCpfSemRestricao");

		System.out.println("\n");
		System.out.println("#### CT02: Exibe CPF sem Restricao ####\n");

		// Given()
		request = RestAssured.given().log().all();
		String cpf = RestricaoCPF.getRandomCpf();
		Response response = getRestricaoResponse(cpf);
		// when()S
		Assertions.assertEquals(HttpStatus.SC_NO_CONTENT, response.statusCode());
		// Then()
		System.out.printf("\n" + "O CPF %s não possui restricao\n", cpf);
	}

	private Response getRestricaoResponse(String cpf) {
		Response response = given().contentType(ContentType.JSON).pathParam("cpf", cpf).when().log().all()
				.get("api/v1/" + RESTRICOES_CPF).then().log().all().extract().response();
		return response;
	}

}

package api;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import io.restassured.RestAssured;

public abstract class AcessoApi {

	@BeforeAll
	public static void acessoApi() {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
		RestAssured.basePath = "api/v1";
	}

	@AfterAll
	public static void cleanUp() {

	}

}

package simulacoes;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import io.restassured.RestAssured;
import io.restassured.response.Response;

//Ordenar os testes com Junit
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RemoverSimulacoes extends SimulacoesBase {
	
    @Test
    void validarExclusaosimulacao() {
    	
		System.out.println("\n");
		System.out.println("#### CT01: Valida exclusão de simulacao cadastrada ####\n");
		
    	Simulacoes simulation = simulacoesDados.selectRandomSimulationInDatabase();
        Response response = deleteRequest(simulation.getId());
        Assertions.assertEquals(HttpStatus.SC_NO_CONTENT, response.statusCode());
    }

    @Test
    void validarExclusaoSimulacaoIdNaoEncontrado() {
    	
		System.out.println("\n");
		System.out.println("#### CT02: Valida exclusão de simulacao com ID não cadastrado ####\n");
    	
        Response response = deleteRequest(simulacoesDados.getNonExistentID());
        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, response.statusCode());
        Assertions.assertEquals("Simulação não encontrada", response.jsonPath().getString("erros"));
    }
    
    private Response deleteRequest(int id) {
        Response response = RestAssured.given().log().all()
                .pathParam("id", id).
                when().log().all()
                .delete(BASE_PATH_SIMULATION + "/{id}").
                then().log().all()
                .extract().response();
        return response;
    }
}

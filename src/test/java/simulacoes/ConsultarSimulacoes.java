package simulacoes;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import io.qameta.allure.Description;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

//Ordenar os testes com Junit
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultarSimulacoes extends SimulacoesBase {

	private static final String BASE_PATH_SIMULATION = "simulacoes";

	@Test
	void consultaSimulacaoCpfNaoEncontrado() {

		System.out.println("\n");
		System.out.println("#### CT01: Consulta simulações (CPF não encontrado) ####\n");

		String cpf = simulacoesDados.getRandomCpf();
		Response response = getRequestSimulation(cpf);
		Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, response.statusCode());
	}

	@Test
	void consultaSimulacaocpfValido() {

		System.out.println("\n");
		System.out.println("#### CT02: Consulta simulações (CPF válido) ####\n");

		Simulacoes simulation = simulacoesDados.selectRandomSimulationInDatabase();
		Response response = getRequestSimulation(simulation.getCpf());
		Simulacoes simulationJsonObject = simulacoesDados.jsonToObject(response.getBody().print());
		Assertions.assertEquals(true, simulation.equals(simulationJsonObject));
	}

	@Test
	@Description("Test Description: Query (GET) the empty simulation base.")
	void consultaSeExisteSimulacoesCadastradas() {

		System.out.println("\n");
		System.out.println("#### CT03: Consulta simulações cadastradas ####\n");

		Response response = getRequestAllSimulations();
		if (response.getBody().print() == "[]") {
			Assertions.assertEquals(HttpStatus.SC_NO_CONTENT, response.statusCode());
		}
		Assertions.assertEquals(HttpStatus.SC_OK, response.statusCode());
	}

	private Response getRequestAllSimulations() {
		Response response = RestAssured.when().get(BASE_PATH_SIMULATION).then().log().all().statusCode(HttpStatus.SC_OK)
				.extract().response();
		return response;
	}

	private Response getRequestSimulation(String cpf) {
		Response response = RestAssured.given().contentType(ContentType.JSON).pathParam("cpf", cpf).log().all().when()
				.log().all().get(BASE_PATH_SIMULATION + "/{cpf}").then().extract().response();
		return response;
	}

}

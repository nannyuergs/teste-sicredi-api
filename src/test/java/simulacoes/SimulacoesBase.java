package simulacoes;

import api.AcessoApi;
import utils.RestricaoCPF;
import utils.SimulacoesDados;

public abstract class SimulacoesBase extends AcessoApi {

	public static final String BASE_PATH_SIMULATION = "simulacoes";
	protected final SimulacoesDados simulacoesDados;
	protected final RestricaoCPF restricaoCPF;

	public SimulacoesBase() {
		simulacoesDados = new SimulacoesDados();
		restricaoCPF = new RestricaoCPF();
	}
}

package simulacoes;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@Feature("Cadastrar Simulacoes")
public class CadastrarSimulacoes extends SimulacoesBase {

	@Test
	void cadastrarNovaSimulacao() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		Response response = postRequest(simulacoes);
		Simulacoes simulacoesJsonObject = simulacoesDados.jsonToObject(response.getBody().print());
		Assertions.assertEquals(HttpStatus.SC_CREATED, response.statusCode());
		Assertions.assertEquals(true, simulacoes.equals(simulacoesJsonObject));
	}

	@Test
	void validarSimulacaoCpfCadastrado() {
		Simulacoes simulacoes = simulacoesDados.selectRandomSimulationInDatabase();
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_CONFLICT, response.statusCode());
		Assertions.assertEquals("CPF já existente", response.jsonPath().getString("erros"));
	}

	@Test
	void validarCadastroSimulacaopfInvalido() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setCpf("9999999999");
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("CPF deve ser um cpf válido", response.jsonPath().getString("erros"));
	}

	@Test
	void validaCadastroSimulacaoCpfEmBranco() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setCpf(null);
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("[cpf:CPF não pode ser vazio]", response.jsonPath().getString("erros"));
	}

	@Test
	void validaCadastroSimulacaoNomeEmBranco() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setNome(null);
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("[nome:Nome não pode ser vazio]", response.jsonPath().getString("erros"));
	}

	@Test
	void validaCadastroSimulacaoEmailEmBranco() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setEmail(null);
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("[email:E-mail não deve ser vazio]", response.jsonPath().getString("erros"));
	}

	@Test
	void validaCadastroSimulacaoEmailInvalido() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setEmail("sicredi.com");
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("[email:E-mail deve ser um e-mail válido]", response.jsonPath().getString("erros"));
	}

	@Test
	void validaCadastroSimulacaoCampoValorMenor() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setValor(999);
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("[valor:Valor deve ser maior ou igual a R$ 1.000]",
				response.jsonPath().getString("erros"));
	}

	@Test
	void validaCadastroSimulacaoCampoValorMaior() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setValor(40001);
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("[valor:Valor deve ser menor ou igual a R$ 40.000]",
				response.jsonPath().getString("erros"));
	}

	@Test
	void validaCadastroSimulacaoCampoParcelaMaior() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setParcelas(1);
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("[parcelas:Parcelas deve ser maior ou igual a 2]",
				response.jsonPath().getString("erros"));
	}

	@Test
	void validaCadastroSimulacaoCampoParcelaMenor() {
		Simulacoes simulacoes = simulacoesDados.generateNewSimulation();
		simulacoes.setParcelas(49);
		Response response = postRequest(simulacoes);
		Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		Assertions.assertEquals("[parcelas:Parcelas deve ser menor ou igual a 48]",
				response.jsonPath().getString("erros"));
	}

	private Response postRequest(Simulacoes simulacoes) {
		Response response = RestAssured.given().contentType(ContentType.JSON).and().body(simulacoes).log().all()
				.when().log().all()
				.post(BASE_PATH_SIMULATION)
				.then().log().all()
				.extract().response();
		return response;
	}

}

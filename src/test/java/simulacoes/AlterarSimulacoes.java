package simulacoes;

import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

//Ordenar os testes com Junit
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlterarSimulacoes extends SimulacoesBase {

	@Test
	void alterarSimulacaoCPFNaoEncontrado() {

		System.out.println("\n");
		System.out.println("#### CT01: Tentando alterar simulaçao de CPF não cadastrado ####\n");

		String cpf = simulacoesDados.generateNonExistentCpf();
		Simulacoes simulation = simulacoesDados.selectRandomSimulationInDatabase();
		Response response = putRequest(cpf, simulation);
		Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, response.statusCode());
	}

	@Test
	void alteraSimulacoesComSucesso() {

		System.out.println("\n");
		System.out.println("#### CT02: Alterar simulacoes existentes ####\n");

		Simulacoes simulation = simulacoesDados.selectRandomSimulationInDatabase();
		Simulacoes newSimulation = simulacoesDados.generateNewSimulation();
		Response response = putRequest(simulation.getCpf(), newSimulation);
		Assertions.assertEquals(HttpStatus.SC_OK, response.statusCode());
	}

	private Response putRequest(String cpf, Simulacoes Simulacoes) {
		Response response = RestAssured.given().contentType(ContentType.JSON).pathParam("cpf", cpf).log().all()
				.body(Simulacoes).when().log().all()
				.put(BASE_PATH_SIMULATION + "/{cpf}").then().extract().response();
		return response;
	}
}
